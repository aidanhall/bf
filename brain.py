def bracket_pass(program, start) -> int:
    """
    Find the index of the matching closing bracket.
    start: The index of the relevant opening bracket.
    """
    index = start + 1
    bracket_depth = 1
    while bracket_depth > 0 and index < len(program):
        if program[index] == '[':
            bracket_depth += 1
        elif program[index] == ']':
            bracket_depth -= 1

        index += 1

    return index - 1


def brain(program):
    memory = [0] * 256
    brackets = []
    pointer = 0
    program_counter = 0
    while program_counter < len(program):
        if program_counter < 0:
            program_counter = len(memory) + program_counter

        if program[program_counter] == '>':
            pointer += 1
        elif program[program_counter] == '<':
            pointer -= 1
        elif program[program_counter] == '+':
            memory[pointer] += 1
        elif program[program_counter] == '-':
            memory[pointer] -= 1
        elif program[program_counter] == '@':
            print(memory[pointer])
        elif program[program_counter] == '.':
            print(chr(memory[pointer]), end='')
        elif program[program_counter] == ',':
            memory[pointer] = int(input('> '))
        elif program[program_counter] == '[':
            if memory[pointer] == 0:
                program_counter = bracket_pass(program, program_counter)
            else:
                brackets.append(program_counter)
        elif program[program_counter] == ']':
            program_counter = brackets.pop() - 1
        elif program[program_counter].isspace():
            pass
        else:
            print("Invalid character at index", program_counter,
                  ":", program[program_counter])
            break

        program_counter += 1


if __name__ == "__main__":
    program = ""
    while program != " ":
        program = input("BF> ")
        if len(program) > 0 and program[0] == 'f':
            program = open(program[1:]).read()
        brain(program)
